<?php
namespace CodeYellow\LaravelHelpers\Eloquent;

function formatCarbonTimestamp(\Carbon\Carbon $carbon, $format)
{
    if ($format && $carbon) {
        return $carbon->format($format);
    }
    
    return $carbon;
}

trait TimestampsTrait
{
    public function getCreatedAt($format = null)
    {
        return formatCarbonTimestamp($this->created_at, $format);
    }

    public function getUpdatedAt($format = null)
    {
        return formatCarbonTimestamp($this->updated_at, $format);
    }

    public function getDeletedAt($format = null)
    {
        return formatCarbonTimestamp($this->deleted_at, $format);
    }
}
