# Eloquent

## TimestampsTrait

Adds 3 functions:

- getCreatedAt($format = null)
- getUpdatedAt($format = null)
- getDeletedAt($format = null)

If called without arguments, returns a Corbon object. You can influence the
return value by setting format:

```$this->getCreatedAt('d-m-Y')```